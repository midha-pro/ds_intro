# Introduction
## Wikipedia defintion
`Data science is an interdisciplinary field that uses scientific methods, processes, algorithms and systems to extract knowledge and insights from noisy, structured and unstructured data 
and apply knowledge and actionable insights from data across a broad range of application domains`

The actual defitnition of Data science can be very broad (also changing with the rapid development of tools and technologies). The field is hard to define in couple of sentences. In general a 
data scientist needs to be well versed in statistical/mathematical algorithms as well as programming/software enginerring skills.

A common phrase amoong this profession

*** A data scientist is a Statistician turned programmer or programmed turened Statistician ***

## Job Titles
While data scientist is the most common title used across indutry, there are other titles common as well
- Data Analyst
- Statistician
- Decision Scientist
- ML engineer

There is no common consensus on the roles and responsibilities. It can vary from company to company.

## Mathematical Foundation
To become a good data scientist one needs to have a basic understanding of the mathematical and statistical algoritms. There are 4 areas which one needs to focus
- Probability & Statistics (bit of Information Theory)
- Linear Algebra (Good understanding of Matrix algebra)
- Multivariable Calculus
- Numerical optimization

## Progarmming foundation
Here are the core areas which one needs to focus
- Programming language (While you can choose any (Python/R/ Matlab/Octave/Java/C++/Julia),Python is the most popular) If you are staring new focus on Python
- Good SQL skills. (This is something one learns more on the job)
### Software Skills
- BI tool (like Tableau/Power BI/Excel)
- Version Control Systems (like Github/Bitbucket or gitlab One should start their own repo)
- Familiarity with data enginerring concepts.
- Familiarity with docker or other environment management ecosystem (conda).

The list of skills can be infinite. What is recommended is to chose a topic of interest (like sports/politics/finance) explore open data sets. Apply data science to the open data sets.
